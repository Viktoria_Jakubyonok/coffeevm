﻿using CoffeeVM.DAL.Models;
using System;

namespace CoffeeVM.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
       IRepository<Transaction> Transactions { get; }
       IRepository<BalancePurse> BalancePurses { get; }
       IRepository<Purse> Purses { get; }
       IRepository<Coin> Coins { get; }
       IRepository<Product> Products { get; }
    }
}
