﻿using Microsoft.EntityFrameworkCore;

namespace CoffeeVM.DAL.DataContext
{
    public class OptionsBuild
    {
        public DbContextOptionsBuilder<ApplicationDatabaseContext> ApplicationOptionsBuilder { get; set; }
        public DbContextOptions<ApplicationDatabaseContext> ApplicationDatabaseContextOptions { get; set; }

        private ApplicationConfiguration _settings { get; set; }

        public OptionsBuild()
        {
            _settings = new ApplicationConfiguration();

            ApplicationOptionsBuilder = new DbContextOptionsBuilder<ApplicationDatabaseContext>();
            ApplicationOptionsBuilder.UseSqlServer(_settings.ApplicationSqlConnectionString);
            ApplicationDatabaseContextOptions = ApplicationOptionsBuilder.Options;
        }
    }
}
