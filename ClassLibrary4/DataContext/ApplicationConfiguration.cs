﻿namespace CoffeeVM.DAL.DataContext
{
    public class ApplicationConfiguration
    {
        public string ApplicationSqlConnectionString { get; set; }

        public ApplicationConfiguration()
        {
            // Строка подключения захардкожена, это не самый лучший вариант, по возможности вынести это в конфиг, и реализовать считывание из него
            // Insert your connection string
            ApplicationSqlConnectionString =
                "Data Source = (localdb)\\mssqllocaldb; Initial Catalog = CoffeeVM; Integrated Security = True;";
        }
    }
}
