﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CoffeeVM.DAL.DataContext
{
    public class ApplicationDatabaseContextFactory : IDesignTimeDbContextFactory<ApplicationDatabaseContext>
    {
        public ApplicationDatabaseContext CreateDbContext(string[] args)
        {
            var appConfig = new ApplicationConfiguration();
            var opsBuilder = new DbContextOptionsBuilder<ApplicationDatabaseContext>();
            opsBuilder.UseSqlServer(appConfig.ApplicationSqlConnectionString);

            return new ApplicationDatabaseContext(opsBuilder.Options);
        }
    }
}
