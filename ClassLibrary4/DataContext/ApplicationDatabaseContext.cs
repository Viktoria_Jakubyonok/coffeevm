﻿using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace CoffeeVM.DAL.DataContext
{
    public class ApplicationDatabaseContext : DbContext
    {
        public static OptionsBuild OptionsBuild = new OptionsBuild();

        public ApplicationDatabaseContext(DbContextOptions<ApplicationDatabaseContext> options)
            : base(options)
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public DbSet<Coin> Coins { get; set; }
        public DbSet<Purse> Purses { get; set; }
        public DbSet<BalancePurse> BalancePurses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
