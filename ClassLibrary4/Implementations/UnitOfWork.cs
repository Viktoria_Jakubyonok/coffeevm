﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using CoffeeVM.DAL.Repositories;
using System;

namespace CoffeeVM.DAL.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;

        private ApplicationDatabaseContext _context;
        private TransactionRepository transactionRepository;
        private BalancePurseRepository balancePurseRepository;
        private PurseRepository purseRepository;
        private CoinRepository coinRepository;
        private ProductRepository productRepository;

        public UnitOfWork()
        {
            _context = new ApplicationDatabaseContext(ApplicationDatabaseContext.OptionsBuild.ApplicationDatabaseContextOptions);
        }

        public IRepository<Transaction> Transactions
        {
            get
            {
                if (transactionRepository == null)
                    transactionRepository = new TransactionRepository(_context);    

                return transactionRepository;
            }
        }

        public IRepository<BalancePurse> BalancePurses
        {
            get
            {
                if (balancePurseRepository == null)
                    balancePurseRepository = new BalancePurseRepository(_context);

                return balancePurseRepository;
            }
        }

        public IRepository<Purse> Purses
        {
            get
            {
                if (purseRepository == null)
                    purseRepository = new PurseRepository(_context);

                return purseRepository;
            }
        }

        public IRepository<Coin> Coins
        {
            get
            {
                if (coinRepository == null)
                    coinRepository = new CoinRepository(_context);

                return coinRepository;
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(_context);

                return productRepository;
            }
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
