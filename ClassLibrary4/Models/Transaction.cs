﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoffeeVM.DAL.Models
{
    public class Transaction
    {
        public long Id { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
