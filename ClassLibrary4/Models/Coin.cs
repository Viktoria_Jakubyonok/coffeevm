﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoffeeVM.DAL.Models
{
    public class Coin
    {
        public Coin()
        {
            BalancePurses = new List<BalancePurse>();
        }
        public int Id { get; set; }

        [Required]
        public int FaceValue { get; set; }

        public ICollection<BalancePurse> BalancePurses { get; set; }
    }
}
