﻿using System.ComponentModel.DataAnnotations;

namespace CoffeeVM.DAL.Models
{
    public class BalancePurse
    {
        public int Id { get; set; }

        public int? PurseId { get; set; }
        public Purse Purse { get; set; }

        public int? CoinId { get; set; }
        public Coin Coin { get; set; }

        [Required]
        public int Count { get; set; }
    }
}
