﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CoffeeVM.DAL.Repositories
{
    public class CoinRepository : IRepository<Coin>
    {
        private readonly ApplicationDatabaseContext _context;

        public CoinRepository(ApplicationDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<Coin> GetList()
        {
            return _context.Coins.ToList();
        }

        public Coin GetItem(int id)
        {
            return _context.Coins.Find(id);
        }

        public void Create(Coin coin)
        {
            _context.Coins.Add(coin);
        }

        public void Update(Coin coin)
        {
            _context.Entry(coin).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var coin = _context.Coins.Find(id);
            if (coin != null)
                _context.Coins.Remove(coin);
        }

        public void Save()
        {
            _context.SaveChanges();
        } 
    }
}
