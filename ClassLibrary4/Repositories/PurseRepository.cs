﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CoffeeVM.DAL.Repositories
{
    public class PurseRepository : IRepository<Purse>
    {
        private ApplicationDatabaseContext _context;

        public PurseRepository(ApplicationDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<Purse> GetList()
        {
            return _context.Purses
                .Include(x => x.BalancePurses)
                .ToList();
        }

        public Purse GetItem(int id)
        {
            return _context.Purses
                .Include(x => x.BalancePurses.Select(bp => bp.Coin))
                .FirstOrDefault(p => p.Id == id);
        }

        public void Create(Purse purse)
        {
            _context.Purses.Add(purse);
        }

        public void Update(Purse purse)
        {
            _context.Entry(purse).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var purse = _context.Purses.Find(id);
            if (purse != null)
                _context.Purses.Remove(purse);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
