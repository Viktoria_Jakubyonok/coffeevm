﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CoffeeVM.DAL.Repositories
{
    public class BalancePurseRepository : IRepository<BalancePurse>
    {
        private ApplicationDatabaseContext _context;

        public BalancePurseRepository(ApplicationDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<BalancePurse> GetList()
        {
            return _context.BalancePurses;
        }

        public BalancePurse GetItem(int id)
        {
            return _context.BalancePurses.Find(id);
        }

        public void Create(BalancePurse balancePurse)
        {
            _context.BalancePurses.Add(balancePurse);
        }

        public void Update(BalancePurse balancePurse)
        {
            _context.Entry(balancePurse).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var balancePurse = _context.BalancePurses.Find(id);

            if (balancePurse != null) 
                _context.BalancePurses.Remove(balancePurse);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
