﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CoffeeVM.DAL.Repositories
{
    public class TransactionRepository : IRepository<Transaction>
    {
        private ApplicationDatabaseContext _context;

        public TransactionRepository(ApplicationDatabaseContext context)
        {
            this._context = context;
        }

        public IEnumerable<Transaction> GetList()
        {
            return _context.Transactions;
        }

        public Transaction GetItem(int id)
        {
            return _context.Transactions.Find(id);
        }

        public void Create(Transaction transaction)
        {
            _context.Transactions.Add(transaction);
        }

        public void Update(Transaction transaction)
        {
            _context.Entry(transaction).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var transaction = _context.Transactions.Find(id);
            if (transaction != null)
                _context.Transactions.Remove(transaction);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
