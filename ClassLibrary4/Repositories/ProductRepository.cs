﻿using CoffeeVM.DAL.DataContext;
using CoffeeVM.DAL.Interfaces;
using CoffeeVM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CoffeeVM.DAL.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private ApplicationDatabaseContext _context;

        public ProductRepository(ApplicationDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> GetList()
        {
            return _context.Products;
        }

        public Product GetItem(int id)
        {
            return _context.Products.Find(id);
        }

        public void Create(Product product)
        {
            _context.Products.Add(product);
        }

        public void Update(Product product)
        {
            _context.Entry(product).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var product = _context.Products.Find(id);
            if (product != null)
                _context.Products.Remove(product);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
