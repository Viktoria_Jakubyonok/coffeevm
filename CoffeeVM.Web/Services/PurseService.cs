﻿using CoffeeVM.Web.Interfaces;
using CoffeeVM.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CoffeeVM.DAL.Interfaces;

namespace CoffeVM.Web.Services
{
    public class PurseService : IPurseService
    {
        private IUnitOfWork Database { get; set; }

        public PurseService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public IEnumerable<PurseDto> GetAll()
        {
            var purseAll = Database.Purses.GetList();

            if (purseAll == null)
            {
                throw new ValidationException("Список кошельков пуст");
            }

            var gettingPurse = purseAll.Select(p => new PurseDto
            {
                Id = p.Id,
                PurseType = p.PurseType,
                Description = p.Description,
                NumberCoins = GetNumberCoin(p.Id).ToList()
            }).ToList();

            return gettingPurse;
        }

        public PurseDto Get(int id)
        {
            var purse = Database.Purses.GetItem(id);

            if (purse == null)
            {
                throw new ValidationException("Не найден кошелек");
            } 

            var gettingPurse = new PurseDto
            { 
                Id = purse.Id, 
                PurseType = purse.PurseType, 
                Description = purse.Description, 
                NumberCoins = GetNumberCoin(id).ToList()
            };

            return gettingPurse;
        }

        public IEnumerable<NumberCoinDto> GetNumberCoin(int purseId)
        {
            try
            {
                //var purse = Database.Purses.GetItem(purseId);

                //if (purse == null)
                //{
                //    throw new ValidationException("Не найден кошелек");
                //}

                //var balancePurses = purse.BalancePurses.Where(bp => bp.PurseId == purseId);

                //if (balancePurses == null || balancePurses.ToList().Count == 0)
                //{
                //    throw new ValidationException();
                //}

                //var numberCoins = balancePurses
                //        .Select(nc => new NumberCoinDto
                //        {
                //            Count = nc.Count,
                //            Coin = new CoinDto
                //            {
                //              Id = nc.Coin.Id,
                //              FaceValue = nc.Coin.FaceValue
                //            }
                //        }).ToList();

                return new List<NumberCoinDto>();
            }
            catch (Exception ex)
            {
                throw;
            }
        }   
    }
}
