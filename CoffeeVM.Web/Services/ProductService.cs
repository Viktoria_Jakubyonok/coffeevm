﻿using CoffeeVM.Web.Interfaces;
using CoffeeVM.Web.Models;
using System;
using System.Collections.Generic;
using CoffeeVM.DAL.Interfaces;

namespace CoffeeVM.Web.Services
{
    public class ProductService : IProductService
    {
        public IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public IEnumerable<ProductDto> GetAll()
        {
            //var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CoffeeVM.Data.Models.Product, ProductDto>()).CreateMapper();
            //return mapper.Map<IEnumerable<CoffeeVM.Data.Models.Product>, List<ProductDto>>(Database.Products.GetList().Where(p => p.Count > 0)).ToList();
            throw new NotImplementedException();
        }
            
        public ProductDto Get(int id)
        {
            var product = Database.Products.GetItem(id);
            var gettingProduct = new ProductDto { Id = product.Id, Name = product.Name, Price = product.Price, Count = product.Count };
            return gettingProduct;
        }
       
    }
}
