﻿using System.Collections.Generic;
using CoffeeVM.DAL.Enums;

namespace CoffeeVM.Web.Models
{
    public class PurseDto
    {
        public int Id { get; set; }
        public PurseType PurseType { get; set; }
        public string Description { get; set; }
        public List<NumberCoinDto> NumberCoins { get; set; }
    }
}
