﻿using System;

namespace CoffeeVM.Web.Models
{
    public class TransactionDto
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public ProductDto Product { get; set; }
    }
}
