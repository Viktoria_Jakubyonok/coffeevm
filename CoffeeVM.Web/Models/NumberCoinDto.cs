﻿namespace CoffeeVM.Web.Models
{
    public class NumberCoinDto
    {
        public CoinDto Coin { get; set; }
        public int Count { get; set; }
    }
}
