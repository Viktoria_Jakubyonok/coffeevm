﻿namespace CoffeeVM.Web.Models
{
    public class CoinDto
    {
        public int Id { get; set; }
        public int FaceValue { get; set; }
    }
}
