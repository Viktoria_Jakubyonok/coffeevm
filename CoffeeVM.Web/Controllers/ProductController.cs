﻿using CoffeeVM.Web.Interfaces;
using CoffeeVM.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CoffeeVM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public IEnumerable<ProductDto> GetAll()
        {
            var purses = _productService.GetAll();

            return purses;
        }

        [HttpGet("{id}")]
        public ProductDto Get(int id)
        {
            var purse = _productService.Get(id);

            return purse;
        }
    }
}