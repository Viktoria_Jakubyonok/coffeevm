﻿using CoffeeVM.Web.Interfaces;
using CoffeeVM.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace CoffeeVM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurseController : ControllerBase
    {
        private readonly IPurseService _purseService;

        public PurseController(IPurseService purseService)
        {
            _purseService = purseService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var purses = _purseService.GetAll();

            // Для примера используется такой набор полей, возвращаем то что нужно
            return Ok(purses.Select(x => new
            {
                Id = x.Id,
                Description = x.Description
            }).ToList());
        }

        [HttpGet("{id}")]
        public PurseDto Get(int id)
        {
            var purse = _purseService.Get(id);

            return purse;
        }
    }
}