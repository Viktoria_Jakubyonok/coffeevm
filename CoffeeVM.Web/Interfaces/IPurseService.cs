﻿using System.Collections.Generic;
using CoffeeVM.Web.Models;

namespace CoffeeVM.Web.Interfaces
{
    public interface IPurseService
    {
        IEnumerable<PurseDto> GetAll();
        PurseDto Get(int id);
        IEnumerable<NumberCoinDto> GetNumberCoin(int purseId);      
    }
}
