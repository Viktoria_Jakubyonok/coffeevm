﻿using System.Collections.Generic;
using CoffeeVM.Web.Models;

namespace CoffeeVM.Web.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDto> GetAll();
        ProductDto Get(int id);
    }
}
