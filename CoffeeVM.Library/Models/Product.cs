﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoffeeVM.Data.Models
{
    public class Product
    {
        public Product()
        {
            Transactions = new List<Transaction>();
        }
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Count { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
