﻿using CoffeeVM.Data.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoffeeVM.Data.Models
{
    public class Purse
    {
        public Purse()
        {
            BalancePurses = new List<BalancePurse>();
        }
        public int Id { get; set; }

        [Required]
        public PurseType PurseType { get; set; }
        public string Description { get; set; }

        public ICollection<BalancePurse> BalancePurses { get; set; }
    }
}
