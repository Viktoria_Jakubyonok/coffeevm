﻿using CoffeeVM.Data.Interface;
using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CoffeeVM.Data.Repository
{
    public class CoinRepository : IRepository<Coin>
    {
        private readonly ApplicationDbContext _context;

        public CoinRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Coin> GetList()
        {
            return _context.Coins.ToList();
        }

        public Coin GetItem(int id)
        {
            return _context.Coins.Find(id);
        }

        public void Create(Coin coin)
        {
            _context.Coins.Add(coin);
        }

        public void Update(Coin coin)
        {
            _context.Entry(coin).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Coin coin = _context.Coins.Find(id);
            if (coin != null) _context.Coins.Remove(coin);
        }

        public void Save()
        {
            _context.SaveChanges();
        } 
    }
}
