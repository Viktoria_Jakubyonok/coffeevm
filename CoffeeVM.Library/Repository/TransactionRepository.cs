﻿using CoffeeVM.Data.Interface;
using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace CoffeeVM.Data.Repository
{
    public class TransactionRepository : IRepository<Transaction>
    {
        private ApplicationDbContext _context;

        public TransactionRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Transaction> GetList()
        {
            return _context.Transactions;
        }

        public Transaction GetItem(int id)
        {
            return _context.Transactions.Find(id);
        }

        public void Create(Transaction transaction)
        {
            _context.Transactions.Add(transaction);
        }

        public void Update(Transaction transaction)
        {
            _context.Entry(transaction).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Transaction transaction = _context.Transactions.Find(id);
            if (transaction != null)
                _context.Transactions.Remove(transaction);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
