﻿using CoffeeVM.Data.Interface;
using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;

//using System.Data.Entity;

namespace CoffeeVM.Data.Repository
{
    public class ProductRepository : IRepository<Product>
    {
        private ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> GetList()
        {
            return _context.Products;
        }

        public Product GetItem(int id)
        {
            return _context.Products.Find(id);
        }

        public void Create(Product product)
        {
            _context.Products.Add(product);
        }

        public void Update(Product product)
        {
            _context.Entry(product).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Product product = _context.Products.Find(id);
            if (product != null)
                _context.Products.Remove(product);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
