﻿using CoffeeVM.Data.Interface;
using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CoffeeVM.Data.Repository
{
    public class PurseRepository : IRepository<Purse>
    {
        private ApplicationDbContext _context;

        public PurseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Purse> GetList()
        {
            return _context.Purses
                .Include(x => x.BalancePurses)
                .ToList();

            //    var purse = new Purse
            //    {
            //        Id = 123,
            //        PurseType = PurseType.User,
            //        BalancePurses = null,
            //        Description = "string"
            //    };

            //    return new List<Purse>
            //    {
            //        purse
            //    };
        }

        public Purse GetItem(int id)
        {
            return _context.Purses
                .Include(x => x.BalancePurses.Select(bp => bp.Coin))
                .FirstOrDefault(p => p.Id == id);

            //return new Purse
            //{
            //    Id = 123,
            //    PurseType = PurseType.User,
            //    BalancePurses = null,
            //    Description = "string"
            //};
        }

        public void Create(Purse purse)
        {
            _context.Purses.Add(purse);
        }

        public void Update(Purse purse)
        {
            _context.Entry(purse).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Purse purse = _context.Purses.Find(id);
            if (purse != null)
                _context.Purses.Remove(purse);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
