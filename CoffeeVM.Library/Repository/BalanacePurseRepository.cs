﻿using CoffeeVM.Data.Interface;
using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace CoffeeVM.Data.Repository
{
    public class BalancePurseRepository : IRepository<BalancePurse>
    {
        private ApplicationDbContext _context;

        public BalancePurseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<BalancePurse> GetList()
        {
            return _context.BalancePurses;
        }

        public BalancePurse GetItem(int id)
        {
            return _context.BalancePurses.Find(id);
        }

        public void Create(BalancePurse balancePurse)
        {
            _context.BalancePurses.Add(balancePurse);
        }

        public void Update(BalancePurse balancePurse)
        {
            _context.Entry(balancePurse).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            BalancePurse balancePurse = _context.BalancePurses.Find(id);

            if (balancePurse != null) 
                _context.BalancePurses.Remove(balancePurse);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
