﻿namespace CoffeeVM.Data.Enums
{
    public enum PurseType : byte
    {
        VM = 1,
        User = 2
    }
}
