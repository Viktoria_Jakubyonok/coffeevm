﻿using CoffeeVM.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;


namespace CoffeeVM.Data
{
    public class ApplicationDbContextInitializer: DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var userPurse = new Purse { PurseType = Data.Enums.PurseType.User, Description = "Кошелек пользователя №1" };
            var VMPurse = new Purse { PurseType = Data.Enums.PurseType.VM, Description = "Деньги в машине №1" };

            context.Purses.AddRange(new List<Purse> { userPurse,VMPurse });
            context.SaveChanges();

            var firstCoin = new Coin { FaceValue = 1 };
            var secondCoin = new Coin { FaceValue = 2 };
            var thirdCoin = new Coin { FaceValue = 5 };
            var fourthCoin = new Coin { FaceValue = 10 };

            context.Coins.AddRange(new List<Coin> { firstCoin, secondCoin, thirdCoin, fourthCoin });
            context.SaveChanges();

            var firstProduct = new Product { Name = "Чай", Price = 13, Count=10 };
            var secondProduct = new Product { Name = "Кофе", Price = 18, Count = 20 };
            var thirdProduct = new Product { Name = "Кофе с молоком", Price = 21, Count = 20 };
            var fourthProduct = new Product { Name = "Сок", Price = 35, Count = 15 };

            context.Products.AddRange(new List<Product> { firstProduct, secondProduct, thirdProduct, fourthProduct });
            context.SaveChanges();

            var firstUserBalancePurse = new BalancePurse
            {
                PurseId = userPurse.Id,
                CoinId = firstCoin.Id,
                Count = 10
            };

            var secondUserBalancePurse = new BalancePurse
            {
                PurseId = userPurse.Id,
                CoinId = secondCoin.Id,
                Count = 30
            };

            var thirdUserBalancePurse = new BalancePurse
            {
                PurseId = userPurse.Id,
                CoinId = thirdCoin.Id,
                Count = 20
            };

            var fourthUserBalancePurse = new BalancePurse
            {
                PurseId = userPurse.Id,
                CoinId = fourthCoin.Id,
                Count = 15
            };

            var firstVMBalancePurse = new BalancePurse
            {
                PurseId = VMPurse.Id,
                CoinId = firstCoin.Id,
                Count = 100
            };

            var secondVMBalancePurse = new BalancePurse
            {
                PurseId = VMPurse.Id,
                CoinId = secondCoin.Id,
                Count = 100
            };

            var thirdVMBalancePurse = new BalancePurse
            {
                PurseId = VMPurse.Id,
                CoinId = thirdCoin.Id,
                Count = 100
            };

            var fourthVMBalancePurse = new BalancePurse
            {
                PurseId = VMPurse.Id,
                CoinId = fourthCoin.Id,
                Count = 100
            };

            context.BalancePurses.AddRange(new List<BalancePurse>
            {
                firstUserBalancePurse,
                secondUserBalancePurse,
                thirdUserBalancePurse,
                fourthUserBalancePurse,
                firstVMBalancePurse,
                secondVMBalancePurse,
                thirdVMBalancePurse,
                fourthVMBalancePurse
            });
            context.SaveChanges();

        }
    }
}
