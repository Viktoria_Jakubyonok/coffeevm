﻿using CoffeeVM.Data.Models;
using System;
using System.Data.Entity;

namespace CoffeeVM.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() 
            //:base("Data Source=DESKTOP-LE2HLJV\\SQLEXPRESS; Initial Catalog=CoffeeVM; Integrated Security=True;")
            :base("DbConnection")
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public DbSet<Coin> Coins { get; set; }
        public DbSet<Purse> Purses { get; set; }
        public DbSet<BalancePurse> BalancePurses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
