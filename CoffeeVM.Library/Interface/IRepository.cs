﻿using System.Collections.Generic;

namespace CoffeeVM.Data.Interface
{
    public interface IRepository<T> 
        where T : class
    {
        IEnumerable<T> GetList();
        T GetItem(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
        void Save();
    }
}
