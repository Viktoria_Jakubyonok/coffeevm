﻿using CoffeeVM.Data.Models;
using System;

namespace CoffeeVM.Data.Interface
{
    public interface IUnitOfWork : IDisposable
    {
       IRepository<Transaction> Transactions { get; }
       IRepository<BalancePurse> BalancePurses { get; }
       IRepository<Purse> Purses { get; }
       IRepository<Coin> Coins { get; }
       IRepository<Product> Products { get; }
    }
}
