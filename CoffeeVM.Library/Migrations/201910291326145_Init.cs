namespace CoffeeVM.Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BalancePurses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurseId = c.Int(),
                        CoinId = c.Int(),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coins", t => t.CoinId)
                .ForeignKey("dbo.Purses", t => t.PurseId)
                .Index(t => t.CoinId)
                .Index(t => t.PurseId);
            
            CreateTable(
                "dbo.Coins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FaceValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Purses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurseType = c.Byte(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ProductId", "dbo.Products");
            DropForeignKey("dbo.BalancePurses", "PurseId", "dbo.Purses");
            DropForeignKey("dbo.BalancePurses", "CoinId", "dbo.Coins");
            DropIndex("dbo.Transactions", new[] { "ProductId" });
            DropIndex("dbo.BalancePurses", new[] { "PurseId" });
            DropIndex("dbo.BalancePurses", new[] { "CoinId" });
            DropTable("dbo.Transactions");
            DropTable("dbo.Products");
            DropTable("dbo.Purses");
            DropTable("dbo.Coins");
            DropTable("dbo.BalancePurses");
        }
    }
}
