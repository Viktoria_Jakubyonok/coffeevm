﻿namespace CoffeeVM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rangeGroupBox = new System.Windows.Forms.GroupBox();
            this.rangeDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chooseProductButton = new System.Windows.Forms.Button();
            this.informationProductLabel = new System.Windows.Forms.Label();
            this.rangeLabel = new System.Windows.Forms.Label();
            this.rangeComboBox = new System.Windows.Forms.ComboBox();
            this.purseGroupBox = new System.Windows.Forms.GroupBox();
            this.informationPurseLabel = new System.Windows.Forms.Label();
            this.purseDataGridView = new System.Windows.Forms.DataGridView();
            this.valueCoinsUserColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countCoinsUserColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coinsValueLabel = new System.Windows.Forms.Label();
            this.makeMoneyButton = new System.Windows.Forms.Button();
            this.coinsValueComboBox = new System.Windows.Forms.ComboBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.amounLabel = new System.Windows.Forms.Label();
            this.remainsButton = new System.Windows.Forms.Button();
            this.calculationGroupBox = new System.Windows.Forms.GroupBox();
            this.payButton = new System.Windows.Forms.Button();
            this.cashVMGroupBox = new System.Windows.Forms.GroupBox();
            this.informationCashierLabel = new System.Windows.Forms.Label();
            this.cashierDataGridView = new System.Windows.Forms.DataGridView();
            this.valueCoinsVMColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countCoinsVMColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleLabel = new System.Windows.Forms.Label();
            this.exitLabel = new System.Windows.Forms.Label();
            this.rangeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeDataGridView)).BeginInit();
            this.purseGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purseDataGridView)).BeginInit();
            this.calculationGroupBox.SuspendLayout();
            this.cashVMGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cashierDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rangeGroupBox
            // 
            this.rangeGroupBox.Controls.Add(this.rangeDataGridView);
            this.rangeGroupBox.Controls.Add(this.chooseProductButton);
            this.rangeGroupBox.Controls.Add(this.informationProductLabel);
            this.rangeGroupBox.Controls.Add(this.rangeLabel);
            this.rangeGroupBox.Controls.Add(this.rangeComboBox);
            this.rangeGroupBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rangeGroupBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rangeGroupBox.Location = new System.Drawing.Point(33, 57);
            this.rangeGroupBox.Name = "rangeGroupBox";
            this.rangeGroupBox.Size = new System.Drawing.Size(427, 178);
            this.rangeGroupBox.TabIndex = 4;
            this.rangeGroupBox.TabStop = false;
            this.rangeGroupBox.Text = "Ассортимент";
            // 
            // rangeDataGridView
            // 
            this.rangeDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.rangeDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.rangeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rangeDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.rangeDataGridView.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.rangeDataGridView.Location = new System.Drawing.Point(156, 53);
            this.rangeDataGridView.Name = "rangeDataGridView";
            this.rangeDataGridView.Size = new System.Drawing.Size(240, 110);
            this.rangeDataGridView.TabIndex = 22;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Товар";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Цена";
            this.Column2.Name = "Column2";
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Кол-во";
            this.Column3.Name = "Column3";
            this.Column3.Width = 50;
            // 
            // chooseProductButton
            // 
            this.chooseProductButton.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.chooseProductButton.FlatAppearance.BorderSize = 0;
            this.chooseProductButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chooseProductButton.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseProductButton.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.chooseProductButton.Location = new System.Drawing.Point(52, 83);
            this.chooseProductButton.Name = "chooseProductButton";
            this.chooseProductButton.Size = new System.Drawing.Size(75, 33);
            this.chooseProductButton.TabIndex = 13;
            this.chooseProductButton.Text = "ВЫБРАТЬ";
            this.chooseProductButton.UseVisualStyleBackColor = false;
            // 
            // informationProductLabel
            // 
            this.informationProductLabel.AutoSize = true;
            this.informationProductLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.informationProductLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.informationProductLabel.Location = new System.Drawing.Point(152, 30);
            this.informationProductLabel.Name = "informationProductLabel";
            this.informationProductLabel.Size = new System.Drawing.Size(154, 20);
            this.informationProductLabel.TabIndex = 21;
            this.informationProductLabel.Text = "Информация о товаре";
            // 
            // rangeLabel
            // 
            this.rangeLabel.AutoSize = true;
            this.rangeLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rangeLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rangeLabel.Location = new System.Drawing.Point(6, 30);
            this.rangeLabel.Name = "rangeLabel";
            this.rangeLabel.Size = new System.Drawing.Size(114, 20);
            this.rangeLabel.TabIndex = 15;
            this.rangeLabel.Text = "Выберете товар";
            // 
            // rangeComboBox
            // 
            this.rangeComboBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.rangeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rangeComboBox.FormattingEnabled = true;
            this.rangeComboBox.Location = new System.Drawing.Point(6, 53);
            this.rangeComboBox.Name = "rangeComboBox";
            this.rangeComboBox.Size = new System.Drawing.Size(121, 24);
            this.rangeComboBox.TabIndex = 19;
            // 
            // purseGroupBox
            // 
            this.purseGroupBox.Controls.Add(this.informationPurseLabel);
            this.purseGroupBox.Controls.Add(this.purseDataGridView);
            this.purseGroupBox.Controls.Add(this.coinsValueLabel);
            this.purseGroupBox.Controls.Add(this.makeMoneyButton);
            this.purseGroupBox.Controls.Add(this.coinsValueComboBox);
            this.purseGroupBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.purseGroupBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.purseGroupBox.Location = new System.Drawing.Point(33, 241);
            this.purseGroupBox.Name = "purseGroupBox";
            this.purseGroupBox.Size = new System.Drawing.Size(427, 187);
            this.purseGroupBox.TabIndex = 13;
            this.purseGroupBox.TabStop = false;
            this.purseGroupBox.Text = "Кошелек";
            // 
            // informationPurseLabel
            // 
            this.informationPurseLabel.AutoSize = true;
            this.informationPurseLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.informationPurseLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.informationPurseLabel.Location = new System.Drawing.Point(152, 26);
            this.informationPurseLabel.Name = "informationPurseLabel";
            this.informationPurseLabel.Size = new System.Drawing.Size(173, 20);
            this.informationPurseLabel.TabIndex = 22;
            this.informationPurseLabel.Text = "Информация о кошельке";
            // 
            // purseDataGridView
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.purseDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.purseDataGridView.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.purseDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.purseDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.purseDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.purseDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.purseDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.valueCoinsUserColumn,
            this.countCoinsUserColumn});
            this.purseDataGridView.Enabled = false;
            this.purseDataGridView.Location = new System.Drawing.Point(156, 49);
            this.purseDataGridView.Name = "purseDataGridView";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.purseDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.purseDataGridView.Size = new System.Drawing.Size(206, 122);
            this.purseDataGridView.TabIndex = 23;
            // 
            // valueCoinsUserColumn
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueCoinsUserColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.valueCoinsUserColumn.HeaderText = "Номинал";
            this.valueCoinsUserColumn.Name = "valueCoinsUserColumn";
            // 
            // countCoinsUserColumn
            // 
            this.countCoinsUserColumn.HeaderText = "Кол-во";
            this.countCoinsUserColumn.Name = "countCoinsUserColumn";
            this.countCoinsUserColumn.Width = 60;
            // 
            // coinsValueLabel
            // 
            this.coinsValueLabel.AutoSize = true;
            this.coinsValueLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.coinsValueLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.coinsValueLabel.Location = new System.Drawing.Point(6, 26);
            this.coinsValueLabel.Name = "coinsValueLabel";
            this.coinsValueLabel.Size = new System.Drawing.Size(125, 20);
            this.coinsValueLabel.TabIndex = 22;
            this.coinsValueLabel.Text = "Выберете купюру";
            // 
            // makeMoneyButton
            // 
            this.makeMoneyButton.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.makeMoneyButton.FlatAppearance.BorderSize = 0;
            this.makeMoneyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.makeMoneyButton.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.makeMoneyButton.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.makeMoneyButton.Location = new System.Drawing.Point(52, 79);
            this.makeMoneyButton.Name = "makeMoneyButton";
            this.makeMoneyButton.Size = new System.Drawing.Size(75, 32);
            this.makeMoneyButton.TabIndex = 22;
            this.makeMoneyButton.Text = "ВНЕСТИ";
            this.makeMoneyButton.UseVisualStyleBackColor = false;
            // 
            // coinsValueComboBox
            // 
            this.coinsValueComboBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.coinsValueComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.coinsValueComboBox.FormattingEnabled = true;
            this.coinsValueComboBox.Location = new System.Drawing.Point(6, 49);
            this.coinsValueComboBox.Name = "coinsValueComboBox";
            this.coinsValueComboBox.Size = new System.Drawing.Size(121, 24);
            this.coinsValueComboBox.TabIndex = 22;
            // 
            // amountTextBox
            // 
            this.amountTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.amountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.amountTextBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amountTextBox.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.amountTextBox.Location = new System.Drawing.Point(10, 49);
            this.amountTextBox.Multiline = true;
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.ReadOnly = true;
            this.amountTextBox.Size = new System.Drawing.Size(202, 38);
            this.amountTextBox.TabIndex = 14;
            this.amountTextBox.Text = "0 руб. ";
            this.amountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // amounLabel
            // 
            this.amounLabel.AutoSize = true;
            this.amounLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.amounLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.amounLabel.Location = new System.Drawing.Point(6, 26);
            this.amounLabel.Name = "amounLabel";
            this.amounLabel.Size = new System.Drawing.Size(123, 20);
            this.amounLabel.TabIndex = 13;
            this.amounLabel.Text = "Внесенная сумма";
            // 
            // remainsButton
            // 
            this.remainsButton.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.remainsButton.FlatAppearance.BorderSize = 0;
            this.remainsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.remainsButton.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.remainsButton.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.remainsButton.Location = new System.Drawing.Point(10, 95);
            this.remainsButton.Name = "remainsButton";
            this.remainsButton.Size = new System.Drawing.Size(78, 35);
            this.remainsButton.TabIndex = 13;
            this.remainsButton.Text = "СДАЧА";
            this.remainsButton.UseVisualStyleBackColor = false;
            // 
            // calculationGroupBox
            // 
            this.calculationGroupBox.Controls.Add(this.payButton);
            this.calculationGroupBox.Controls.Add(this.remainsButton);
            this.calculationGroupBox.Controls.Add(this.amounLabel);
            this.calculationGroupBox.Controls.Add(this.amountTextBox);
            this.calculationGroupBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculationGroupBox.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.calculationGroupBox.Location = new System.Drawing.Point(466, 241);
            this.calculationGroupBox.Name = "calculationGroupBox";
            this.calculationGroupBox.Size = new System.Drawing.Size(224, 187);
            this.calculationGroupBox.TabIndex = 15;
            this.calculationGroupBox.TabStop = false;
            this.calculationGroupBox.Text = "Расчет";
            // 
            // payButton
            // 
            this.payButton.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.payButton.FlatAppearance.BorderSize = 0;
            this.payButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.payButton.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payButton.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.payButton.Location = new System.Drawing.Point(133, 95);
            this.payButton.Name = "payButton";
            this.payButton.Size = new System.Drawing.Size(79, 35);
            this.payButton.TabIndex = 15;
            this.payButton.Text = "ОПЛАТИТЬ";
            this.payButton.UseVisualStyleBackColor = false;
            // 
            // cashVMGroupBox
            // 
            this.cashVMGroupBox.Controls.Add(this.informationCashierLabel);
            this.cashVMGroupBox.Controls.Add(this.cashierDataGridView);
            this.cashVMGroupBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cashVMGroupBox.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cashVMGroupBox.Location = new System.Drawing.Point(466, 57);
            this.cashVMGroupBox.Name = "cashVMGroupBox";
            this.cashVMGroupBox.Size = new System.Drawing.Size(224, 178);
            this.cashVMGroupBox.TabIndex = 17;
            this.cashVMGroupBox.TabStop = false;
            this.cashVMGroupBox.Text = "Касса";
            // 
            // informationCashierLabel
            // 
            this.informationCashierLabel.AutoSize = true;
            this.informationCashierLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.informationCashierLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.informationCashierLabel.Location = new System.Drawing.Point(6, 30);
            this.informationCashierLabel.Name = "informationCashierLabel";
            this.informationCashierLabel.Size = new System.Drawing.Size(153, 20);
            this.informationCashierLabel.TabIndex = 22;
            this.informationCashierLabel.Text = "Информация по кассе";
            // 
            // cashierDataGridView
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.cashierDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.cashierDataGridView.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.cashierDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cashierDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cashierDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.cashierDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cashierDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.valueCoinsVMColumn,
            this.countCoinsVMColumn});
            this.cashierDataGridView.Enabled = false;
            this.cashierDataGridView.Location = new System.Drawing.Point(6, 53);
            this.cashierDataGridView.Name = "cashierDataGridView";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowFrame;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cashierDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.cashierDataGridView.Size = new System.Drawing.Size(206, 120);
            this.cashierDataGridView.TabIndex = 22;
            // 
            // valueCoinsVMColumn
            // 
            this.valueCoinsVMColumn.HeaderText = "Номинал";
            this.valueCoinsVMColumn.Name = "valueCoinsVMColumn";
            // 
            // countCoinsVMColumn
            // 
            this.countCoinsVMColumn.HeaderText = "Кол-во";
            this.countCoinsVMColumn.Name = "countCoinsVMColumn";
            this.countCoinsVMColumn.Width = 60;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.titleLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.titleLabel.Location = new System.Drawing.Point(236, 23);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(243, 31);
            this.titleLabel.TabIndex = 15;
            this.titleLabel.Text = "VM по продаже кофе";
            // 
            // exitLabel
            // 
            this.exitLabel.AutoSize = true;
            this.exitLabel.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exitLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exitLabel.Location = new System.Drawing.Point(661, 23);
            this.exitLabel.Name = "exitLabel";
            this.exitLabel.Size = new System.Drawing.Size(29, 31);
            this.exitLabel.TabIndex = 18;
            this.exitLabel.Text = "X\r\n";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(711, 450);
            this.Controls.Add(this.exitLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.cashVMGroupBox);
            this.Controls.Add(this.calculationGroupBox);
            this.Controls.Add(this.purseGroupBox);
            this.Controls.Add(this.rangeGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "CoffeeVM";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.rangeGroupBox.ResumeLayout(false);
            this.rangeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeDataGridView)).EndInit();
            this.purseGroupBox.ResumeLayout(false);
            this.purseGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purseDataGridView)).EndInit();
            this.calculationGroupBox.ResumeLayout(false);
            this.calculationGroupBox.PerformLayout();
            this.cashVMGroupBox.ResumeLayout(false);
            this.cashVMGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cashierDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox rangeGroupBox;
        private System.Windows.Forms.GroupBox purseGroupBox;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.Label amounLabel;
        private System.Windows.Forms.Button remainsButton;
        private System.Windows.Forms.GroupBox calculationGroupBox;
        private System.Windows.Forms.GroupBox cashVMGroupBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label exitLabel;
        private System.Windows.Forms.ComboBox rangeComboBox;
        private System.Windows.Forms.Button chooseProductButton;
        private System.Windows.Forms.Label informationProductLabel;
        private System.Windows.Forms.Label rangeLabel;
        private System.Windows.Forms.Label informationPurseLabel;
        private System.Windows.Forms.DataGridView purseDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueCoinsUserColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countCoinsUserColumn;
        private System.Windows.Forms.Label coinsValueLabel;
        private System.Windows.Forms.Button makeMoneyButton;
        private System.Windows.Forms.ComboBox coinsValueComboBox;
        private System.Windows.Forms.Label informationCashierLabel;
        private System.Windows.Forms.DataGridView cashierDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueCoinsVMColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countCoinsVMColumn;
        private System.Windows.Forms.Button payButton;
        private System.Windows.Forms.DataGridView rangeDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

