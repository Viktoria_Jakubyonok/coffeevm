﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CoffeeVM
{
    public class PurseGetter
    {
        // Insert your localhost address
        private static readonly string _basePurseApiUrl = @"https://localhost:xxxxx/api/purse";

        public PurseGetter()
        { }

        public List<PurseViewModel> GetAllPurses()
        {
            var url = CreateGetAllPursesUrl(_basePurseApiUrl);
            var response = SendRequest(url);

            return JsonConvert.DeserializeObject<List<PurseViewModel>>(response);
        }

        public string CreateGetAllPursesUrl(string baseUrl)
        {
            return baseUrl;
        }

        public string CreateGetPurseUrl(string url, int purseId)
        {
            throw new NotImplementedException();
        }

        // Этот метод вынесни в общий какой-то сервис, для работы с Http
        public static string SendRequest(string url)
        {
            try
            {
                string resp;
                var request = (HttpWebRequest)WebRequest.Create(url);

                using (var response = request.GetResponseAsync().Result)
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            resp = reader.ReadToEnd();
                        }
                    }
                }

                return resp;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }
    }
}
